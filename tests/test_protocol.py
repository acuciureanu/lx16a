from unittest import mock

from lx16a.protocol import Protocol


def test_length():
    res = Protocol.length(b"\x00\x00")
    assert res == (2 + 3)


def test_checksum():
    res = Protocol.checksum(0, 0, 0, bytes())
    assert res == 255
    res = Protocol.checksum(1, 5, 1, b"\x00\x00")
    assert res == 248


def test_build_packet_parse_packet():
    p = Protocol.build_packet(1, 1, b"\x00\x00")
    assert isinstance(p, bytes)
    response = Protocol.parse_packet(p)
    assert response.id == 1
    assert response.command == 1
    assert response.data == b"\x00\x00"
    assert response.length > 0
    assert response.checksum != 0


@mock.patch("io.RawIOBase")
def test_command(serial):
    p = Protocol(serial=serial)
    p.command(1, 1, 32)
    serial.write.assert_called_once_with(b"\x55\x55\x01\x05\x01\x20\x00\xd8")
    p.command(1, 1)
    serial.write.assert_called_with(b"\x55\x55\x01\x03\x01\xfa")


@mock.patch("io.RawIOBase")
def test_query(serial):
    serial.read.return_value = b"\x55\x55\x01\x05\x01\x20\x00\xd8"
    p = Protocol(serial=serial)
    res = p.query(1, 28)
    serial.write.assert_called_with(b"\x55\x55\x01\x03\x1c\xdf")
    assert res["id"] == 1
