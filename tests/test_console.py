from unittest import mock

from click.testing import CliRunner

from lx16a.console import position, servo, cli


@mock.patch("serial.Serial")
@mock.patch("lx16a.console.Servo.set_position")
@mock.patch("lx16a.console.Servo.get_position")
def test_set_position(get_pos, set_pos, serial):
    runner = CliRunner()
    result = runner.invoke(position, ["--id=1", "500"])
    assert result.exit_code == 0
    set_pos.assert_called_once()
    set_pos.assert_called_with(500)
    result = runner.invoke(position, ["--id=1"])
    assert result.exit_code == 0
    get_pos.assert_called_once()
    get_pos.assert_called_with()


def test_servo():
    runner = CliRunner()
    result = runner.invoke(servo)
    assert result.exit_code == 0
    assert result.output != None


def test_cli():
    runner = CliRunner()
    result = runner.invoke(cli)
    assert result.exit_code == 0
